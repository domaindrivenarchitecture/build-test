#  dda_devops_build
#  Copyright 2019 meissa GmbH.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from os import environ
from subprocess import run
from pybuilder.core import init, task, use_plugin, Author
from ddadevops import *


use_plugin("python.core")
use_plugin("copy_resources")
use_plugin("filter_resources")
# use_plugin("python.unittest")
# use_plugin("python.coverage")
use_plugin("python.distutils")

# use_plugin("python.install_dependencies")

default_task = "dev"

name = "ddadevops"
MODULE = "not-used"
PROJECT_ROOT_PATH = "."
version = "4.0.1-dev"
summary = "tools to support builds combining gopass, terraform, dda-pallet, aws & hetzner-cloud"
description = __doc__
authors = [Author("meissa GmbH", "buero@meissa-gmbh.de")]
url = "https://repo.prod.meissa.de/meissa/dda-devops-build"
requires_python = ">=3.10"  # CHECK IF NEW VERSION EXISTS
license = "Apache Software License"
