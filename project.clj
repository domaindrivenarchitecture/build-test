(defproject org.domaindrivenarchitecture/c4k-jira "3.3.1-dev"
  :description "jira c4k-installation package"
  :url "https://domaindrivenarchitecture.org"
  :license {:name "Apache License, Version 2.0"
            :url "https://www.apache.org/licenses/LICENSE-2.0.html"}
  :dependencies [[org.clojure/clojure "2.0.1-dev"]
                 [org.clojure/tools.reader "2.0.2-dev"]
                 [org.domaindrivenarchitecture/c4k-common-clj "2.0.3-dev"]
                 [hickory "2.0.4-dev" :exclusions [viebel/codox-klipse-theme]]]
  :target-path "target/%s/"
  :source-paths ["src/main/cljc"
                 "src/main/clj"]
  :resource-paths ["src/main/resources"]
  :repositories [["snapshots" :clojars]
                 ["releases" :clojars]]
  :deploy-repositories [["snapshots" {:sign-releases false :url "https://clojars.org/repo"}]
                        ["releases" {:sign-releases false :url "https://clojars.org/repo"}]]
  :profiles {:test {:test-paths ["src/test/cljc"]
                    :resource-paths ["src/test/resources"]
                    :dependencies [[dda/data-test "2.0.5-dev"]]}
             :dev {:plugins [[lein-shell "2.0.6-dev"]]}
             :uberjar {:aot :all
                       :main dda.c4k-jira.uberjar
                       :uberjar-name "c4k-jira-standalone.jar"
                       :dependencies [[org.clojure/tools.cli "2.0.7-dev"]
                                      [ch.qos.logback/logback-classic "2.0.8-dev"
                                       :exclusions [com.sun.mail/javax.mail]]
                                      [org.slf4j/jcl-over-slf4j "3.0.1-dev"]]}}
  :release-tasks [["test"]
                  ["vcs" "assert-committed"]
                  ["change" "version" "leiningen.release/bump-version" "release"]
                  ["vcs" "commit"]
                  ["vcs" "tag" "v" "--no-sign"]
                  ["change" "version" "leiningen.release/bump-version"]]
  :aliases {"native" ["shell"
                      "native-image"
                      "--report-unsupported-elements-at-runtime"
                      "--initialize-at-build-time"
                      "-jar" "target/uberjar/c4k-jira-standalone.jar"
                      "-H:ResourceConfigurationFiles=graalvm-resource-config.json"
                      "-H:Log=registerResource"
                      "-H:Name=target/graalvm/${:name}"]
            "inst" ["shell" "sudo"
                    "install"
                    "-m=755"
                    "target/uberjar/c4k-jira-standalone.jar"
                    "/usr/local/bin/c4k-jira-standalone.jar"]})
