This repo shows examples how dda-devops-build (https://repo.prod.meissa.de/meissa/dda-devops-build) can be used.
As a starting point, you may want to have a look at file [build.py](build.py) in this repo 
and the tasks which are defined in this file. 

# Usage

## Examples

### Build

You can trigger the build-task by 
```
pyb dev
```

### Releases

With task `patch` you can create a patch-level release with one command, i.e. perform the following actions:
* create a commit for a patch-leve release version
  * e.g. if current project version is 1.1.1-dev, the update version will be the release version 1.1.1
  * the commit contains all build files specified in the build.py (primary as well as secondary build files), 
    where the primary build file contains the version number to be used for all build files
* create a tag for the release commit (e.g. tag 1.1.1 for the corresponding commit)
* a second commit for the next snapshot version (1.1.2-dev)
* push both commits and the tag to the remote git repo

```
pyb patch
```

Similarly you can create a minor or major version with:

```
pyb minor
```
resp.
```
pyb major
```



# Release Testing
## NoRelease

```
pyb dev prepare_release tag_bump_and_push_release
```
## Release

```
pyb [patch|minor|major] prepare_release tag_bump_and_push_release
```

## Development & mirrors

Development happens at: https://repo.prod.meissa.de/meissa/build-test

Mirrors are:

* https://gitlab.com/domaindrivenarchitecture/build-test (issues and PR, CI)

For more details about our repository model see: https://repo.prod.meissa.de/meissa/federate-your-repos

## License

Copyright © 2021 meissa GmbH
Licensed under the [Apache License, Version 2.0](LICENSE) (the "License")
