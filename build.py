import os
from os import environ
from pybuilder.core import task, init
from ddadevops import *

name = "buildtest"
MODULE = "release"
PROJECT_ROOT_PATH = "."

version = "4.0.1-dev"


@init
def initialize0(project):
    """
    to avoid prompt for gopass if no artifacts need to be uploaded
    usage: with option "-E ng" , e.g. "pyb -E artifacts patch_local"
    """
    os.environ["RELEASE_ARTIFACT_TOKEN"] = "dummy"  # avoids prompt for RELEASE_ARTIFACT_TOKEN


@init(environments=["artifacts"])
def initialize1(project):
    """
    prompt for gopass if no artifacts need to be uploaded
    usage: with option "-E artifacts" , e.g. "pyb -E artifacts patch_local"
    """
    del os.environ["RELEASE_ARTIFACT_TOKEN"]


@init
def initialize2(project):
    input = {
        "name": name,
        "module": MODULE,
        "stage": "notused",
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": [],
        "mixin_types": ["RELEASE"],
        "release_primary_build_file": "build_to_test.py",
        "release_secondary_build_files": ["package.json", "project.clj", "build.gradle", "build.py"],
        "release_artifacts": ["README.md"],
        "release_artifact_server_url": "https://repo.prod.meissa.de",
        "release_organisation": "meissa",
        "release_repository_name": name,
    }

    project.build_depends_on("ddadevops>=4.10.7")
    build = ReleaseMixin(project, input)
    build.initialize_build_dir()


@task
def patch(project):
    """
    updates version to next patch level, creates a tag, creates new SNAPSHOT version,
    commits build files (primary and secondary) and pushes to remote
    """
    linttest(project, "PATCH")
    release(project)


@task
def patch_local(project):
    """
    updates version to next patch level,
    commits build files (primary and secondary), DOES NOT push to remote
    """
    linttest(project, "PATCH")
    prepare(project)


@task
def minor(project):
    """
    updates version to next minor level, creates a tag, creates new SNAPSHOT version,
    commits build files (primary and secondary) and pushes to remote
    """
    linttest(project, "MINOR")
    release(project)


@task
def major(project):
    """
    updates version to next major level, creates a tag, creates new SNAPSHOT version,
    commits build files (primary and secondary) and pushes to remote
    """
    linttest(project, "MAJOR")
    release(project)


@task
def dev(project):
    linttest(project, "NONE")


@task
def prepare(project):
    build = get_devops_build(project)
    build.prepare_release()


@task
def tag(project):
    build = get_devops_build(project)
    build.tag_bump_and_push_release()


@task
def publish_artifacts(project):
    """ creates a release in <release_artifact_server_url> and uploads artifacts """
    build = get_devops_build(project)
    print("PROTECTED_TOKEN_EXAMPLE" in environ)
    print(environ.get("PROTECTED_TOKEN_EXAMPLE"))
    build.publish_artifacts()


def release(project):
    prepare(project)
    tag(project)


def linttest(project, release_type):
    build = get_devops_build(project)
    build.update_release_type(release_type)
